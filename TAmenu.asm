.MODEL SMALL
.CODE

ORG 100h

MENU:
    JMP Proses                                                               
    Judul1  DB  "                            RUMAH MAKAN KHAS SUNDA                        ",10d,10d,10d,13d,'$'  

    Judul2  DB  "|           MENU        |           TAMBAHAN        |        HARGA       |",10d,13d,'$'
    Nama1   DB  "|  AYAM KAMPUNG BAKAR   |  NASI+LALAPAN KOMPLIT     |      RP.80.000     |",10d,13d,'$'
    Nama2   DB  "|  AYAM NEGRI BAKAR     |  NASI+LALAPAN KOMPLIT     |      RP.95.000     |",10d,13d,'$'
    Nama3   DB  "|  AYAM KAMPUNG GORENG  |         ESTEH+NASI        |      RP.78.000     |",10d,13d,'$'
    Nama4   DB  "|  AYAM NEGRI GORENG    |         ESTEH+NASI        |      RP.65.000     |",10d,13d,'$'
    Nama5   DB  "|     SEMUR JENGKOL     |     NASI+ESTEH MANIS      |      RP.30.000     |",10d,13d,'$'
    
    
proses:
    MOV AH,09H 
    LEA DX,Judul1
    INT 21h
    

    LEA DX,Judul2
    INT 21h
    
    LEA DX,Nama1
    INT 21h
    
    LEA DX,Nama2
    INT 21h
    
    LEA DX,Nama3
    INT 21h
    
    LEA DX,Nama4
    INT 21h
    
    LEA DX,Nama5
    INT 21h
    
    INT 20h
    
END PROSES
